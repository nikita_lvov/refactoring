package refactoring.newrefactoring;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Программа по последовательному поиску номера числа в рандомном массиве целых чисел
 *
 * @author Nikita Lvov 18it18
 */
public class SequentialSearch {

    private static final int MIN_NUMBER_IN_ARRAY = -25;
    private static final int NUMBER_RANGE = 51;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] array = new int[inputArrayLength()];

        fillRandomArray(array);

        System.out.println(Arrays.toString(array));

        System.out.println("Введите число, которое вы хотите найти в массиве");
        int enteredNumber = scanner.nextInt();

        int result = searchElementIndex(enteredNumber, array);

        System.out.println((result == -1) ? "Нет такого значения." : "Номер элемента в последовательности: " + result);
    }

    /**
     * Метод, реализующий заполнение массива рандомными целыми числами
     * от {@code MIN_NUMBER_IN_ARRAY} до {@code NUMBER_RANGE}
     *
     * @param array массив целых чисел
     */
    private static void fillRandomArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = MIN_NUMBER_IN_ARRAY + (int) (Math.random() * NUMBER_RANGE);
        }
    }

    /**
     * Метод, реализующий ввод длинны массива
     *
     * @return целое число, характиризующие длинну массива
     */
    private static int inputArrayLength() {
        Scanner scanner = new Scanner(System.in);
        int arrayLength;

        do {
            System.out.println("Введите длинну массива");
            arrayLength = scanner.nextInt();
        } while (arrayLength < 1);

        return arrayLength;
    }

    /**
     * Метод, реализующий последовательный поиск чисел в массиве
     *
     * @param enteredNumber заданное целое число для поиска
     * @param array         массив целых чисел
     * @return результат поиска в зависимости от переданного числа
     * -1 - если числа нет в массиве
     * {@code numberIndex}(индекс числа), если оно есть в массиве
     */
    private static int searchElementIndex(int enteredNumber, int[] array) {
        for (int numberIndex = 0; numberIndex < array.length; numberIndex++) {
            if (array[numberIndex] == enteredNumber) {
                return numberIndex;
            }
        }
        return -1;
    }
}
